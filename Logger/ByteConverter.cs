﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    class ByteConverter
    {
        public static Byte[] Serialize(List<Byte[]> data)
        {
            Byte[] result = new Byte[data.Count * data[0].Length];
            int i = 0;
            foreach(var litem in data)
            {
                foreach (var item in litem)
                {
                    result[i] = item;
                    i++;
                }
            }
            return result;
        }

        public static List<Byte[]> Deserialize(Byte[] data, int offset)
        {
            List<Byte[]> result = new List<Byte[]>();
            int datalength = data.Length, datamod = datalength % offset;

            

            for (int i = 0; i < datalength/ offset; i++)
            {
                Byte[] tmp = new byte[offset];
                for (int j = 0; j < offset; j++)
                {
                    tmp[j] = data[offset * i + j];
                }
                result.Add(tmp);
            }

            if (datamod != 0)
            {
                Byte[] tmp = new byte[offset];
                for (int j = 0; j < datamod; j++)
                {
                    tmp[j] = data[offset * (datalength / offset) + j];
                }
                for (int j = datamod; j < offset; j++)
                {
                    tmp[j] = 0;
                }
                result.Add(tmp);
            }

            return result;
        }

    }
}
