﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Logger
{
    class Program
    {
        // Задание 1 тест
        static void Task1test()
        {
            Logger log = new Logger(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "dataset.xml");
            log.ReadFile(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\log.txt");
        }

        // Задание 2 тест
        static void Task2test()
        {
            Byte[] b = { 1, 2, 3, 1, 1, 1, 2, 3, 1, 2, 5, 3, 4, 2, 3, 2, 5, 7, 5, 4, 4, 2, 2, 3, 1, 1, 2, 2, 3, 5, 6 };
            foreach (var item in b)
            {
                Console.Write(item);
            }
            Console.Write("\n");

            List<Byte[]> l = ByteConverter.Deserialize(b, 4);
            foreach (var litem in l)
            {
                foreach (var item in litem)
                {
                    Console.Write(item);
                }
                Console.Write("\n");
            }
            b = ByteConverter.Serialize(l);
            foreach (var item in b)
            {
                Console.Write(item);
            }
            Console.Write("\n");
        }

        static void Main(string[] args)
        {
            // Задание 1 - два слэша в строке 52
            // Задание 2 - один слэш в строке 52

            //* 

            // Задание 1 тест
            Task1test();

            /*/

            // Задание 2 тест
            Task2test();

            //*/

            Console.ReadKey();
        }
    }
}
