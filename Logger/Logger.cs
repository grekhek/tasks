﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Logger
{
    class Logger
    {
        private static XDocument xml;

        // Загрушаем xml
        public Logger(string directory, string inFileName)
        {
            xml = XDocument.Load(directory + "\\" + inFileName);
        }

        // Определяется тип сообщения. 
        // Если цвет в столбце Color желтый(Yellow), то выдается сообщение-предупреждение. 
        // Если цвет красный(Red), выдается сообщение-ошибка
        // В остальных случаях выдается успешное сообщение
        private static string GetMessage(string flag, int id)
        {
            string code, result = "Reading id " + id.ToString();
            switch (flag)
            {
                case "Yellow":
                    code = "WARNING: ";
                    result += " - Color is yellow!";
                    break;
                case "Red":
                    code = "ERROR: ";
                    result += " - Color is red!";
                    break;
                default:
                    code = "SUCCESS: ";
                    result += ".";
                    break;
            }
            return code + result;
        }

        // Чтение и обработка xml с записью в файл-лог сообщений
        public void ReadFile(string filepath)
        {
            var ps = xml.Descendants("record");
            using (StreamWriter file = new StreamWriter(filepath))
            {
                foreach (var p in ps)
                {
                    var person = new 
                    {
                        Id = p.Element("id").Value,
                        FirstName = p.Element("first_name").Value,
                        LastName = p.Element("last_name").Value,
                        Email = p.Element("email").Value,
                        Gender = p.Element("gender").Value,
                        IpAddress = p.Element("ip_address").Value,
                        Color = p.Element("color").Value,
                    };
                    string message = GetMessage(person.Color, Convert.ToInt32(person.Id));
                    file.WriteLine(message);
                    Console.WriteLine(person.Id + " " + person.FirstName + " " + person.LastName + " " + person.Email + " " + person.Gender + " " + person.IpAddress + " " + person.Color);
                }
            }
        }
    }
}
